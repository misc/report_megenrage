import requests
import re
import sys

import unittest
class TestStrippingMethods(unittest.TestCase):
    def run_tests(self, method, tests):
        for t in tests:
            self.assertEqual(method(t['avant']), t['apres'])

    def test_strip_models(self):
        tests = [{
            'avant': '* {{en}} [http://www.cliki.net/index CLiki], wiki sur Common Lisp',
            'apres': '*  [http://www.cliki.net/index CLiki], wiki sur Common Lisp',
        },{
            'avant': '''{{Infobox Langage de programmation
| couleur boîte = black
| nom = {{blanc|Lisp}}
}}
Lisp''',
            'apres': '\nLisp',
        }]
 
        self.run_tests(strip_models, tests)


    def test_strip_links(self):
        tests = [{
            'avant': '* [[Paul Graham#Arc|Arc]], dérivé très épuré, créé par [[Paul Graham]]',
            'apres': '* Arc, dérivé très épuré, créé par Paul Graham'
        }, {
            'avant': '[http://clisp.cons.org/ CLISP, une implémentation Common Lisp en licence libre GPL]',
            'apres': ' CLISP, une implémentation Common Lisp en licence libre GPL'
        }]

        self.run_tests(strip_links, tests)

    def tests_strip_html_markup(self):

        tests = [{
            'avant': 'Pouet<ref>coin.</ref>',
            'apres': 'Pouet'
        }, {
            'avant': '''On peut aussi écrire plus efficacement (voir récursion terminale) :
<syntaxhighlight lang="lisp">
(defun factorial (n &optional (acc 1))
  "Calcule la factorielle de l entier n."
  (if (<= n 1)
      acc
    (factorial (- n 1) (* acc n))))
</syntaxhighlight>''',
            'apres': 'On peut aussi écrire plus efficacement (voir récursion terminale) :'
        }]

        self.run_tests(strip_html_markup, tests)

    def tests_strip_various_markup(self):

        tests = [{
            'avant': '=== Pouet ==',
            'apres': ' Pouet ',
        }, {
            'avant': "''' plop ***",
            'apres': ' plop ',
        }]

        self.run_tests(strip_various_markup, tests)

def get_wiki_api(wiki):
    return 'https://%s.wikipedia.org/w/api.php' % wiki

def get_page(page, wiki='en'):
    params = {
        'action': 'query',
        'prop': 'revisions',
        'titles': page,
        'rvslots': '*',
        'rvprop': 'content',
        'formatversion': '2',
        'format': 'json',
    }

    r = requests.get(get_wiki_api(wiki), params=params)
    return r.json()['query']['pages'][0]['revisions'][0]['slots']['main']['content']


def strip_models(page):
    # retire {{ }}
    old_page = ''
    while old_page != page:
        old_page = page
        page = re.sub('\{\{[^\}\{]*\}\}', '', page, flags=re.M)
    return page

def strip_links(page):
    # replace [[Paul Graham#Arc|Arc]] by Arc
    page = re.sub(r'\[\[[^]]+\|([^]]+)\]\]', r'\1', page, flags=re.M)
    page = re.sub(r'\[\[([^]]+)\]\]', r'\1', page, flags=re.M)
    page = re.sub(r'\[http\S*(\s[^]]+)\]', r'\1', page, flags=re.M)
    return page


def strip_cr(page):
    return re.sub("\n", " ", page, flags=re.M)

def strip_html_markup(page):
    page = strip_cr(page)
    for i in ['ref','code', 'syntaxhighlight','tt']:
        r = '<%s[^>]*>.*?</%s\s*>' % (i,i) 
        page = re.sub(r, '', page, flags=re.M)
    return page

def strip_various_markup(page):
    #for i in '*', '=', "'":
    old_page = ''
    while old_page != page:
        old_page = page
        for i in '=', "'", '*':
            for j in range(5,1, -1):
                page = page.replace(j*i, '')
    
    return page 

def remove_punctation(page):
    for i in '*', '.', ',', ':', ';', '(', ')', '»', '%', '«', "'", '—':
        page = page.replace(i, ' ')

    page = page.replace('  ', ' ')

    return page

def strip_wiki_markup(page):
    page = strip_models(page)
    # replace [[Paul Graham#Arc|Arc]] by Arci
    page = strip_links(page)
    page = strip_html_markup(page)
    page = strip_various_markup(page)
    page = remove_punctation(page)
    return page

def get_words(page):
    words = {}
    for i in page.split():
        i = i.lower()
        if i in words:
            words[i] += 1
        else:
            words[i] = 1

    return words

def compare_il_elle(page):
    w = get_words(get_page(page, 'fr'))
    nb_il = w.get('il', 0)
    nb_elle = w.get('elle', 0)
    print("Il: %s\nElle: %s" % (nb_il, nb_elle))

if __name__ == '__main__':
    unittest.main()


