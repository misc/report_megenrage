import sys
import lib
import requests
import urllib.parse

pronouns = {
    'fr': {
        'f': 'elle',
        'm': 'il',
    },
    'en': {
        'f': 'she',
        'm': 'he',
    },
    'de': {
        'f': 'sie',
        'm': 'er',
    }
}


items = {
    'f': 'Q1052281',
    'm': 'Q2449503',
} 
        

query =  """SELECT DISTINCT ?item ?link WHERE {{
  ?item wdt:P31 wd:Q5 ;
        wdt:P21 wd:{gender} .
  ?link schema:about ?item ;
        schema:isPartOf <https://{lang}.wikipedia.org/> ;
        schema:inLanguage "{lang}" .
}}"""

lang = sys.argv[1]

header = False

def print_misgendering(page, lang, f, m):
    global header
    if not header:
        print("Misgendering on %s:" % lang)
        header = True
    print(" %s: %s '%s' and %s '%s'" % (page, f, pronouns[lang]['f'], m, pronouns[lang]['m'] ))
    
for g in ['f', 'm']:

    payload = {
        "format": "json",
        "query": query.format(gender=items[g], lang=lang), 
    }

    
    r = requests.get('https://query.wikidata.org/sparql', params=payload).json()['results']['bindings']
    for i in r:
        page = urllib.parse.unquote(i['link']['value'].split('/')[-1])

        try:
            w = lib.get_words(lib.get_page(page, lang))
        except KeyError as e:
            print("Error getting " + page)
            print(i)
            w = {}
        nb_m = w.get(pronouns[lang]['m'], 0)
        nb_f = w.get(pronouns[lang]['f'], 0)
        if g == 'f':
            if nb_m > nb_f:
                print_misgendering(page, lang, nb_f, nb_m)
            
        if g == 'm':
            if nb_f > nb_m:
                print_misgendering(page, lang, nb_f, nb_m)
